package com.example.jenkins.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	
	@Autowired
	TestService testService;

	@GetMapping("/")
	public String hello() {
		
		return "Hello World";
		
	}
	
	@GetMapping("/api/courses")
	public List courses() {
		
		return testService.courses();
	}
	
}
