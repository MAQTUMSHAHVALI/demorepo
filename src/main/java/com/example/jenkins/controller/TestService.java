package com.example.jenkins.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TestService {

	public List courses() {
		
		ArrayList<String> courses=new ArrayList();
		
		courses.add("JAVA");
		courses.add("Spring boot");
		courses.add("jenkins");
		courses.add("REACT");
		courses.add("Nodejs");
		
		return courses;
	}
}
